<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;

class PertanyaanController extends Controller
{
    public function index()
    {
        $data = DB::table('pertanyaan')->get();
        return view('pertanyaan.index', compact('data'));
    }

    public function create()
    {
        return view('pertanyaan.create');
    }

    public function store(Request $request)
    {
        $request->validate([
            'title' => 'required',
            'body' => 'required'
        ]);
        $query = DB::table('pertanyaan')->insert([
            'judul' => $request['title'],
            'isi' => $request['body'],
            'jawaban_tepat_id' => 1,
            'profil_id' => 1
        ]);
        return redirect('/pertanyaan');
    }

    public function show($pertanyaan_id)
    {
        $data = DB::table('pertanyaan')->where('id', $pertanyaan_id)->first();
        return view('pertanyaan.show', compact('data'));
    }

    public function edit($pertanyaan_id)
    {
        $data = DB::table('pertanyaan')->where('id', $pertanyaan_id)->first();
        return view('pertanyaan.edit', compact('data'));
    }

    public function update($pertanyaan_id, Request $request)
    {
        $request->validate([
            'title' => 'required',
            'body' => 'required'
        ]);
        $query = DB::table('pertanyaan')
            ->where('id', $pertanyaan_id)
            ->update([
                'judul' => $request['title'],
                'isi' => $request['body']
            ]);
        return redirect('/pertanyaan');
    }

    public function destroy($pertanyaan_id)
    {
        $query = DB::table('pertanyaan')->where('id', $pertanyaan_id)->delete();
        return redirect('/pertanyaan');
    }
}

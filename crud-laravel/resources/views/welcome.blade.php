<!DOCTYPE html>
<html lang="{{ str_replace('_', '-', app()->getLocale()) }}">
    <head>
        <meta charset="utf-8">
        <meta name="viewport" content="width=device-width, initial-scale=1">

        <title>Laravel</title>

        <!-- Fonts -->
        <link href="https://fonts.googleapis.com/css?family=Nunito:200,600" rel="stylesheet">

        <!-- Styles -->
        <style>
            html, body {
                background-color: #fff;
                color: #636b6f;
                font-family: 'Nunito', sans-serif;
                font-weight: 200;
                height: 100vh;
                margin: 0;
            }
            button {
                height: 100px;
                width: 200px;
                border-radius: 10px;
                background-color: #3176e7;
                color: white;
                font-weight: bold;
                font-size: 15px;
                cursor: pointer;
            }
            button:hover {
                background-color: #194da1;
            }
        </style>
    </head>
    <body>
        <div style="box-sizing: border-box; height: 100vh; display: flex; justify-content: center; align-items: center">
            <a href="/pertanyaan">
                <button>Go To Pertanyaan</button>
            </a>
        </div>
    </body>
</html>

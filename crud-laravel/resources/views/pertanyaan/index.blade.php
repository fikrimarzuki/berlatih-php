@extends('pertanyaan.master')

@section('content')

<div style="display: flex; flex-direction: column; align-items: center;">
    <a href="/pertanyaan/create" class="btn btn-primary" style="margin-bottom: 20px">Tambah</a>
    <table class="table">
        <thead class="thead-light">
            <tr>
                <th scope="col">#</th>
                <th scope="col">Title</th>
                <th scope="col">Body</th>
                <th scope="col">Actions</th>
            </tr>
        </thead>
        <tbody>
            @forelse ($data as $key=>$value)
                <tr>
                    <td>{{$key + 1}}</th>
                    <td>{{$value->judul}}</td>
                    <td>{{$value->isi}}</td>
                    <td style="display: flex; align-items: center">
                        <div>
                            <a href="/pertanyaan/{{$value->id}}" class="btn btn-info">Show</a>
                        </div>
                        <div>
                            <a href="/pertanyaan/{{$value->id}}/edit" class="btn btn-primary">Edit</a>
                        </div>
                        <form action="/pertanyaan/{{$value->id}}" method="POST">
                            @csrf
                            @method('DELETE')
                            <input type="submit" class="btn btn-danger my-1" value="Delete">
                        </form>
                    </td>
                </tr>
            @empty
                <tr colspan="3">
                    <td>No data</td>
                </tr>  
            @endforelse              
        </tbody>
    </table>
</div>

@endsection
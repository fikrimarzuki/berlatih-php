@extends('pertanyaan.master')

@section('content')

<div style="display: flex; flex-direction: column; align-items: center">
  <h1>DETAIL</h1>
  <h2>Title: {{ $data->judul }}</h2>
  <h2>Body: {{ $data->isi }}</h2>
  <a href="/pertanyaan">
    <button style="height: 40px; width: 100px; border-radius: 5px">Back</button>
  </a>
</div>

@endsection
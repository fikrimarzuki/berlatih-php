<footer style="display: flex; justify-content: center; position: fixed; bottom: 0; height: 50px; width: 100%;">
  <div>
    <strong>Copyright &copy; 2014-2019 <a href="http://adminlte.io">AdminLTE.io</a>.</strong> All rights
    reserved.
  </div>
</footer>